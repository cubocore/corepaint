/*
    *
    * This file is a part of CorePaint.
    * A paint app for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This file is taken from EasyPaint https://github.com/Gr1N/EasyPaint
    * Suitable modifications have been made to meet the needs of CorePaint.
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#ifndef ABSTRACTSELECTION_H
#define ABSTRACTSELECTION_H

#include <QPainter>

#include "abstractinstrument.h"
#include "math.h"

QT_BEGIN_NAMESPACE
class QUndoStack;
class ImageArea;
QT_END_NAMESPACE


class AbstractSelection : public AbstractInstrument {
    Q_OBJECT

public:
    explicit AbstractSelection(QObject *parent = 0);

    void mousePressEvent(QMouseEvent *event, ImageArea &imageArea);
    void mouseMoveEvent(QMouseEvent *event, ImageArea &imageArea);
    void mouseReleaseEvent(QMouseEvent *event, ImageArea &imageArea);
    void clearSelection(ImageArea &imageArea);
    void saveImageChanges(ImageArea &);

    virtual void startSelection(ImageArea &imageArea) = 0;
    virtual void startResizing(ImageArea &imageArea) = 0;
    virtual void startMoving(ImageArea &imageArea) = 0;
    virtual void startAdjusting(ImageArea &imageArea) = 0;
    virtual void select(ImageArea &imageArea) = 0;
    virtual void resize(ImageArea &imageArea) = 0;
    virtual void move(ImageArea &imageArea) = 0;
    virtual void completeSelection(ImageArea &imageArea) = 0;
    virtual void completeResizing(ImageArea &imageArea) = 0;
    virtual void completeMoving(ImageArea &imageArea) = 0;
    virtual void clear() = 0;
    virtual void showMenu(ImageArea &imageArea) = 0;

protected:
    void drawBorder(ImageArea &imageArea);
    void updateCursor(QMouseEvent *event, ImageArea &imageArea);

    QPoint mBottomRightPoint, mTopLeftPoint, mMoveDiffPoint;
    bool mIsPaint, mIsSelectionExists, mIsSelectionMoving, mIsSelectionResizing, mIsImageSelected,
         mIsMouseMoved, mIsSelectionAdjusting;
    int mHeight, mWidth;
    Qt::MouseButton mButton;

};

#endif // ABSTRACTSELECTION_H
